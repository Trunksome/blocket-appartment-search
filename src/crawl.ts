import url from "url";
import $ from "cheerio";
import moment from "moment";
import puppeteer from "puppeteer-extra";
import StealthPlugin from "puppeteer-extra-plugin-stealth";

import { Listing } from "./listing";
import { FullListing } from "./fullListing";
import { Browser, Page } from "puppeteer";

export const launchBrowser = async (): Promise<Browser> => {
  console.info(`launch browser...`);
  const browser = await puppeteer.use(StealthPlugin()).launch({
    headless: process.env.HEADLESS === "true",
    args: ["--disable-dev-shm-usage"],
  });
  return browser;
};

export const openNewTab = async (browser: Browser): Promise<Page> => {
  console.info(`open new tab...`);
  const page = await browser.newPage();
  page.setDefaultNavigationTimeout(parseInt(process.env.DEFAULT_TIMEOUT));
  return page;
};

const getHtml = async (page: Page, urlToCrawl: string): Promise<String> => {
  // console.info(`go to ${urlToCrawl}...`);
  const html = await page
    .goto(urlToCrawl, { waitUntil: "domcontentloaded" })
    .then(async function () {
      await page.waitFor(3000);
      return page.content();
    });
  // await page.screenshot({ path: "screenshot.png" });
  return html;
};

export const login = async (page: Page) => {
  await page.goto("https://www.blocket.se/", { waitUntil: "domcontentloaded" });
  await page.click(`div[class^="MenuIcon__LoginWrapper"] button`, {
    clickCount: 1,
    delay: 40,
  });
  page.click(`div[class^="MenuIcon__LoginWrapper"] button`, {
    clickCount: 1,
    delay: 40,
  });
  await page.waitForNavigation({
    timeout: parseInt(process.env.DEFAULT_TIMEOUT),
    waitUntil: "domcontentloaded",
  });
  await page.focus(`input[type="email"]`);
  await page.keyboard.type(process.env.BLOCKET_EMAIL, { delay: 20 });

  await page.focus(`input[type="password"]`);
  await page.keyboard.type(process.env.BLOCKET_PASSWORD, { delay: 20 });

  await page.click(`#CredRemember`, {
    clickCount: 1,
    delay: 40,
  });

  page.click(`button[type="submit"]`, {
    clickCount: 1,
    delay: 40,
  });
  await page.waitForNavigation({
    timeout: parseInt(process.env.DEFAULT_TIMEOUT),
  });
};

export const applyForListing = async (page: Page, listing: FullListing) => {
  await page.goto(listing.applyUrl);

  const recipient = listing.renterName.split(" ")[0];

  const applyText = `Dear ${recipient},\nI just came across your ad for this apartment, and it is exactly what I am looking for.\nAt the moment I work as a product manager in Munich, but in Mid-August I am going to move to Göteborg to begin my masters degree at Chalmers.\nMy previous landlords know me as a friendly and reliable tenant - you can rest assured that your apartment will be in good hands with me.\n\nI look forward to hearing from you,\nSimon\n\n+4915161465992`;

  await page.focus(`#homeOffer-message`);
  await page.keyboard.type(applyText, { delay: 15 });

  await page.click(
    `.InputDuration .date-container .second-radiobutton-container .radiobutton`,
    {
      clickCount: 1,
      delay: 40,
    }
  );

  const idealMoveInDate = moment(process.env.IDEAL_MOVE_IN_DATE);
  const currentMoment = moment().month();
  const monthsDiff = Math.max(0, idealMoveInDate.month() - currentMoment);

  for (let i = 0; i < monthsDiff; i++) {
    await page.click(`.rdr-MonthAndYear-button.next`, {
      clickCount: 1,
      delay: 200,
    });
  }

  await page.evaluate(() => {
    document.querySelectorAll(".rdr-Day").forEach((day) => {
      day.setAttribute("day", day.innerHTML);
    });
  });

  await page.click(`[day="${idealMoveInDate.date()}"]`, {
    clickCount: 1,
    delay: 40,
  });

  await page.click(
    `.InputDuration .date-container + .date-container .radiobutton`,
    {
      clickCount: 1,
      delay: 40,
    }
  );

  page.click(`.Submit .button`, {
    clickCount: 1,
    delay: 40,
  });
  await page.waitForNavigation({
    timeout: parseInt(process.env.DEFAULT_TIMEOUT),
  });
  page.click(`.SignupHandler .button`, {
    clickCount: 1,
    delay: 40,
  });
  await page.waitForNavigation({
    timeout: parseInt(process.env.DEFAULT_TIMEOUT),
  });
  await page.waitFor(5000);
};

export const getListings = async (
  page: Page,
  targetUrl: string
): Promise<Listing[]> => {
  try {
    const html = await getHtml(page, targetUrl);

    const results = $(
      `div[class^="SearchResults__SearchResultsWrapper"]`,
      html
    );

    const listings: Listing[] = [];
    results[0].children.forEach((listEntry) => {
      const title = $(`h2`, listEntry).text();
      const entryAElement = $(`h2 a`, listEntry)[0];

      if (entryAElement) {
        const listingUrl = entryAElement.attribs["href"];

        const path = url.parse(listingUrl).pathname;
        const splitPath = path.split("/");
        const id =
          splitPath[splitPath.length - 2] +
          "/" +
          splitPath[splitPath.length - 1];
        // console.log({ id, path });
        listings.push(new Listing(id, title, listingUrl));
      }
    });
    return listings;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export const getFullListing = async (
  page: Page,
  listing: Listing
): Promise<FullListing> => {
  const randomWaitTime = Math.floor(Math.random() * 3000);
  await page.waitFor(randomWaitTime);

  const html = await getHtml(page, listing.url);

  const homeSummary = $(`.home-summary`, html)[0];
  const matchInfo = $(`.MatchInfo`, homeSummary)[0];

  // const title = $(`.desktop-header-info h1 span`, html)[0].children[0].data;

  const priceString = $(".home-value span span", matchInfo.children[0])[0]
    .children[0].data;
  const price = parseInt(priceString.replace(/ /g, ""));

  const sizeNode = $(".home-value", matchInfo.children[1])[0];
  let size = "";
  sizeNode.children.forEach((child) => {
    size += child.children[0].data;
  });

  const moveInDateNode = $("[datetime]", matchInfo.children[2])[0];
  const moveInDate = moveInDateNode
    ? moment(moveInDateNode.attribs["datetime"]).format()
    : null;

  const moveOutDateNode = $("[datetime]", matchInfo.children[3])[0];
  const moveOutDate = moveOutDateNode
    ? moment(moveOutDateNode.attribs["datetime"]).format()
    : null;

  const renterName = $(`.ProfileName`, html)[0].children[0].data;
  const applyUrl =
    "https://bostad.blocket.se" +
    $(`.SimpleApplyButton a`, html)[0].attribs["href"];

  return new FullListing(
    listing.id,
    listing.title,
    listing.url,
    price,
    size,
    moveInDate,
    moveOutDate,
    renterName,
    applyUrl,
    moment().format()
  );
};
