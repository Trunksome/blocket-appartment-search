import moment from "moment";

export class FullListing {
  id: string;
  title: string;
  url: string;
  price: number;
  size: string;
  moveInDate: string;
  moveOutDate: string;
  renterName: string;
  applyUrl: string;
  scannedAt: string;

  constructor(
    id: string,
    title: string,
    url: string,
    price: number,
    size: string,
    moveInDate: string,
    moveOutDate: string,
    renterName: string,
    applyUrl: string,
    scannedAt: string
  ) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.price = price;
    this.size = size;
    this.moveInDate = moveInDate;
    this.moveOutDate = moveOutDate;
    this.renterName = renterName;
    this.applyUrl = applyUrl;
    this.scannedAt = scannedAt;
  }

  isInteresting(idealMoveInDate: string, minMonths: number): boolean {
    const currentMoment = moment();
    const idealMoveInMoment = moment(idealMoveInDate);

    const idealMoveInIsCloseToCurrent =
      currentMoment.isAfter(moment(idealMoveInMoment).subtract(31, "days")) ||
      currentMoment.isAfter(idealMoveInMoment);

    let goodMoveInDate = false;
    if (!this.moveInDate) {
      goodMoveInDate = idealMoveInIsCloseToCurrent;
    } else {
      const beginningOfMonth = moment(idealMoveInMoment).date(1);
      const twoWeeksBefore = moment(idealMoveInMoment).subtract(15, "days");
      const beforeDate = beginningOfMonth.isBefore(twoWeeksBefore)
        ? beginningOfMonth
        : twoWeeksBefore;

      const twoWeeksAfter = moment(idealMoveInMoment).add(15, "days");

      goodMoveInDate = moment(this.moveInDate).isBetween(
        beforeDate,
        twoWeeksAfter
      );
    }

    let goodMoveOutDate = false;
    if (!this.moveOutDate) {
      goodMoveOutDate = true;
    } else {
      const moveOutMoment = moment(this.moveOutDate);
      goodMoveOutDate = moveOutMoment.isAfter(
        moment(this.moveInDate).add(minMonths, "months")
      );
    }

    // console.log({
    //   goodMoveInDate,
    //   goodMoveOutDate,
    //   url: this.url,
    // });
    return goodMoveInDate && goodMoveOutDate;
  }
}
