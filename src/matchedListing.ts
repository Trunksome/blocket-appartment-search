import moment from "moment";
import { FullListing } from "./fullListing";

export class MatchedListing {
  listing: FullListing;
  foundAt: string;

  constructor(listing: FullListing) {
    this.listing = listing;
    this.foundAt = moment().format();
  }
}
