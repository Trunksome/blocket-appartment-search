const MongoClient = require("mongodb").MongoClient;
const slack = require("slack-notify")(process.env.SLACK_HOOK);
import moment from "moment";

import { Listing } from "./listing";
import { FullListing } from "./fullListing";
import { MatchedListing } from "./matchedListing";
import {
  launchBrowser,
  openNewTab,
  getListings,
  getFullListing,
  login,
  applyForListing,
} from "./crawl";

const targetUrl: string = `https://www.blocket.se/annonser/hela_sverige/bostad/lagenheter?cg=3020&mre=${parseInt(
  process.env.MAX_SEK
)}&q=g%C3%B6teborg&st=u`;

const idealMoveInDate = process.env.IDEAL_MOVE_IN_DATE;
// const idealMoveInDate = "2020-05-01T00:00:00+02:00";
const minMonths = parseInt(process.env.MIN_MONTHS_AVAILABLE);

export const searchForAppartment = async () => {
  const niceMoveInMoment = moment(idealMoveInDate).format("LL");
  console.info("");
  console.info(`--- ${moment().format("LLL")} ---`);
  console.info(
    `Looking for an appartment with ideal move-in-date ${niceMoveInMoment}`
  );

  const mongoClient = new MongoClient(process.env.DB_CONNECTION, {
    useUnifiedTopology: true,
  });

  let client;
  let browser;
  let tab;

  try {
    client = await mongoClient.connect();
    browser = await launchBrowser();
    tab = await openNewTab(browser);
    // console.info("Connected successfully to database");
    const db = client.db("db");
    const listingsCollection = db.collection("listings");
    const matchedListingsCollection = db.collection("matchedListings");

    // debug
    // await listingsCollection.deleteMany({});
    // await matchedListingsCollection.deleteMany({});

    const listings: Listing[] = await getListings(tab, targetUrl);
    //console.log(listings);

    for (const listing of listings) {
      const dbEntry = await listingsCollection.findOne({ id: listing.id });

      if (dbEntry) {
        console.info(`${listing.id} already in database; ending search`);
        break;
      }

      if (!dbEntry) {
        const fullListing: FullListing = await getFullListing(tab, listing);

        if (!fullListing) continue;

        console.info(`add new listing ${listing.id} to database`);
        await listingsCollection.insertOne(fullListing);

        const isInteresting = fullListing.isInteresting(
          idealMoveInDate,
          minMonths
        );

        if (isInteresting) {
          console.info(`--- ITS A MATCH! ---`);
          console.info(`${fullListing.url}`);
          console.info(`--------------------`);
          const newMatch = new MatchedListing(fullListing);

          await matchedListingsCollection.insertOne(newMatch);
          slack.send({
            text: `Its a match! :tada:\n<${newMatch.listing.url}|${
              newMatch.listing.title
            }>\n:dollar: ${newMatch.listing.price} SEK\n:arrow_right: ${moment(
              newMatch.listing.moveInDate
            ).format("LL")}`,
          });
        }
      }
    }

    console.info("Done checking all listings.");
  } catch (error) {
    console.error(error);
  } finally {
    if (browser) browser.close();
    if (client) client.close();
    console.info("----------------");
  }
};
