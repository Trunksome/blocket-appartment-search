import moment from "moment";
import { searchForAppartment } from "./search";

searchForAppartment();
setInterval(() => {
  if (
    moment().hour() < parseInt(process.env.MAX_HOUR) &&
    moment().hour() > parseInt(process.env.MIN_HOUR)
  ) {
    searchForAppartment();
  }
  // }, 1000 * 60 * 30);
}, parseInt(process.env.SEARCH_INTERVAL_MS));
