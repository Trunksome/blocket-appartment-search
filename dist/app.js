"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
const search_1 = require("./search");
search_1.searchForAppartment();
setInterval(() => {
    if (moment_1.default().hour() < parseInt(process.env.MAX_HOUR) &&
        moment_1.default().hour() > parseInt(process.env.MIN_HOUR)) {
        search_1.searchForAppartment();
    }
    // }, 1000 * 60 * 30);
}, parseInt(process.env.SEARCH_INTERVAL_MS));
//# sourceMappingURL=app.js.map