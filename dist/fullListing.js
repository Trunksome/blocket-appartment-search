"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
class FullListing {
    constructor(id, title, url, price, size, moveInDate, moveOutDate, renterName, applyUrl, scannedAt) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.price = price;
        this.size = size;
        this.moveInDate = moveInDate;
        this.moveOutDate = moveOutDate;
        this.renterName = renterName;
        this.applyUrl = applyUrl;
        this.scannedAt = scannedAt;
    }
    isInteresting(idealMoveInDate, minMonths) {
        const currentMoment = moment_1.default();
        const idealMoveInMoment = moment_1.default(idealMoveInDate);
        const idealMoveInIsCloseToCurrent = currentMoment.isAfter(moment_1.default(idealMoveInMoment).subtract(31, "days")) ||
            currentMoment.isAfter(idealMoveInMoment);
        let goodMoveInDate = false;
        if (!this.moveInDate) {
            goodMoveInDate = idealMoveInIsCloseToCurrent;
        }
        else {
            const beginningOfMonth = moment_1.default(idealMoveInMoment).date(1);
            const twoWeeksBefore = moment_1.default(idealMoveInMoment).subtract(15, "days");
            const beforeDate = beginningOfMonth.isBefore(twoWeeksBefore)
                ? beginningOfMonth
                : twoWeeksBefore;
            const twoWeeksAfter = moment_1.default(idealMoveInMoment).add(15, "days");
            goodMoveInDate = moment_1.default(this.moveInDate).isBetween(beforeDate, twoWeeksAfter);
        }
        let goodMoveOutDate = false;
        if (!this.moveOutDate) {
            goodMoveOutDate = true;
        }
        else {
            const moveOutMoment = moment_1.default(this.moveOutDate);
            goodMoveOutDate = moveOutMoment.isAfter(moment_1.default(this.moveInDate).add(minMonths, "months"));
        }
        // console.log({
        //   goodMoveInDate,
        //   goodMoveOutDate,
        //   url: this.url,
        // });
        return goodMoveInDate && goodMoveOutDate;
    }
}
exports.FullListing = FullListing;
//# sourceMappingURL=fullListing.js.map