"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
class MatchedListing {
    constructor(listing) {
        this.listing = listing;
        this.foundAt = moment_1.default().format();
    }
}
exports.MatchedListing = MatchedListing;
//# sourceMappingURL=matchedListing.js.map