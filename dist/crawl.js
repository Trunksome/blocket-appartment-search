"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const url_1 = __importDefault(require("url"));
const cheerio_1 = __importDefault(require("cheerio"));
const moment_1 = __importDefault(require("moment"));
const puppeteer_extra_1 = __importDefault(require("puppeteer-extra"));
const puppeteer_extra_plugin_stealth_1 = __importDefault(require("puppeteer-extra-plugin-stealth"));
const listing_1 = require("./listing");
const fullListing_1 = require("./fullListing");
exports.launchBrowser = () => __awaiter(void 0, void 0, void 0, function* () {
    console.info(`launch browser...`);
    const browser = yield puppeteer_extra_1.default.use(puppeteer_extra_plugin_stealth_1.default()).launch({
        headless: process.env.HEADLESS === "true",
        args: ["--disable-dev-shm-usage"],
    });
    return browser;
});
exports.openNewTab = (browser) => __awaiter(void 0, void 0, void 0, function* () {
    console.info(`open new tab...`);
    const page = yield browser.newPage();
    page.setDefaultNavigationTimeout(parseInt(process.env.DEFAULT_TIMEOUT));
    return page;
});
const getHtml = (page, urlToCrawl) => __awaiter(void 0, void 0, void 0, function* () {
    // console.info(`go to ${urlToCrawl}...`);
    const html = yield page
        .goto(urlToCrawl, { waitUntil: "domcontentloaded" })
        .then(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield page.waitFor(3000);
            return page.content();
        });
    });
    // await page.screenshot({ path: "screenshot.png" });
    return html;
});
exports.login = (page) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.goto("https://www.blocket.se/", { waitUntil: "domcontentloaded" });
    yield page.click(`div[class^="MenuIcon__LoginWrapper"] button`, {
        clickCount: 1,
        delay: 40,
    });
    page.click(`div[class^="MenuIcon__LoginWrapper"] button`, {
        clickCount: 1,
        delay: 40,
    });
    yield page.waitForNavigation({
        timeout: parseInt(process.env.DEFAULT_TIMEOUT),
        waitUntil: "domcontentloaded",
    });
    yield page.focus(`input[type="email"]`);
    yield page.keyboard.type(process.env.BLOCKET_EMAIL, { delay: 20 });
    yield page.focus(`input[type="password"]`);
    yield page.keyboard.type(process.env.BLOCKET_PASSWORD, { delay: 20 });
    yield page.click(`#CredRemember`, {
        clickCount: 1,
        delay: 40,
    });
    page.click(`button[type="submit"]`, {
        clickCount: 1,
        delay: 40,
    });
    yield page.waitForNavigation({
        timeout: parseInt(process.env.DEFAULT_TIMEOUT),
    });
});
exports.applyForListing = (page, listing) => __awaiter(void 0, void 0, void 0, function* () {
    yield page.goto(listing.applyUrl);
    const recipient = listing.renterName.split(" ")[0];
    const applyText = `Dear ${recipient},\nI just came across your ad for this apartment, and it is exactly what I am looking for.\nAt the moment I work as a product manager in Munich, but in Mid-August I am going to move to Göteborg to begin my masters degree at Chalmers.\nMy previous landlords know me as a friendly and reliable tenant - you can rest assured that your apartment will be in good hands with me.\n\nI look forward to hearing from you,\nSimon\n\n+4915161465992`;
    yield page.focus(`#homeOffer-message`);
    yield page.keyboard.type(applyText, { delay: 15 });
    yield page.click(`.InputDuration .date-container .second-radiobutton-container .radiobutton`, {
        clickCount: 1,
        delay: 40,
    });
    const idealMoveInDate = moment_1.default(process.env.IDEAL_MOVE_IN_DATE);
    const currentMoment = moment_1.default().month();
    const monthsDiff = Math.max(0, idealMoveInDate.month() - currentMoment);
    for (let i = 0; i < monthsDiff; i++) {
        yield page.click(`.rdr-MonthAndYear-button.next`, {
            clickCount: 1,
            delay: 200,
        });
    }
    yield page.evaluate(() => {
        document.querySelectorAll(".rdr-Day").forEach((day) => {
            day.setAttribute("day", day.innerHTML);
        });
    });
    yield page.click(`[day="${idealMoveInDate.date()}"]`, {
        clickCount: 1,
        delay: 40,
    });
    yield page.click(`.InputDuration .date-container + .date-container .radiobutton`, {
        clickCount: 1,
        delay: 40,
    });
    page.click(`.Submit .button`, {
        clickCount: 1,
        delay: 40,
    });
    yield page.waitForNavigation({
        timeout: parseInt(process.env.DEFAULT_TIMEOUT),
    });
    page.click(`.SignupHandler .button`, {
        clickCount: 1,
        delay: 40,
    });
    yield page.waitForNavigation({
        timeout: parseInt(process.env.DEFAULT_TIMEOUT),
    });
    yield page.waitFor(5000);
});
exports.getListings = (page, targetUrl) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const html = yield getHtml(page, targetUrl);
        const results = cheerio_1.default(`div[class^="SearchResults__SearchResultsWrapper"]`, html);
        const listings = [];
        results[0].children.forEach((listEntry) => {
            const title = cheerio_1.default(`h2`, listEntry).text();
            const entryAElement = cheerio_1.default(`h2 a`, listEntry)[0];
            if (entryAElement) {
                const listingUrl = entryAElement.attribs["href"];
                const path = url_1.default.parse(listingUrl).pathname;
                const splitPath = path.split("/");
                const id = splitPath[splitPath.length - 2] +
                    "/" +
                    splitPath[splitPath.length - 1];
                // console.log({ id, path });
                listings.push(new listing_1.Listing(id, title, listingUrl));
            }
        });
        return listings;
    }
    catch (error) {
        console.error(error);
        return [];
    }
});
exports.getFullListing = (page, listing) => __awaiter(void 0, void 0, void 0, function* () {
    const randomWaitTime = Math.floor(Math.random() * 3000);
    yield page.waitFor(randomWaitTime);
    const html = yield getHtml(page, listing.url);
    const homeSummary = cheerio_1.default(`.home-summary`, html)[0];
    const matchInfo = cheerio_1.default(`.MatchInfo`, homeSummary)[0];
    // const title = $(`.desktop-header-info h1 span`, html)[0].children[0].data;
    const priceString = cheerio_1.default(".home-value span span", matchInfo.children[0])[0]
        .children[0].data;
    const price = parseInt(priceString.replace(/ /g, ""));
    const sizeNode = cheerio_1.default(".home-value", matchInfo.children[1])[0];
    let size = "";
    sizeNode.children.forEach((child) => {
        size += child.children[0].data;
    });
    const moveInDateNode = cheerio_1.default("[datetime]", matchInfo.children[2])[0];
    const moveInDate = moveInDateNode
        ? moment_1.default(moveInDateNode.attribs["datetime"]).format()
        : null;
    const moveOutDateNode = cheerio_1.default("[datetime]", matchInfo.children[3])[0];
    const moveOutDate = moveOutDateNode
        ? moment_1.default(moveOutDateNode.attribs["datetime"]).format()
        : null;
    const renterName = cheerio_1.default(`.ProfileName`, html)[0].children[0].data;
    const applyUrl = "https://bostad.blocket.se" +
        cheerio_1.default(`.SimpleApplyButton a`, html)[0].attribs["href"];
    return new fullListing_1.FullListing(listing.id, listing.title, listing.url, price, size, moveInDate, moveOutDate, renterName, applyUrl, moment_1.default().format());
});
//# sourceMappingURL=crawl.js.map