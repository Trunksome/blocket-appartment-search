"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const MongoClient = require("mongodb").MongoClient;
const slack = require("slack-notify")(process.env.SLACK_HOOK);
const moment_1 = __importDefault(require("moment"));
const matchedListing_1 = require("./matchedListing");
const crawl_1 = require("./crawl");
const targetUrl = `https://www.blocket.se/annonser/hela_sverige/bostad/lagenheter?cg=3020&mre=${parseInt(process.env.MAX_SEK)}&q=g%C3%B6teborg&st=u`;
const idealMoveInDate = process.env.IDEAL_MOVE_IN_DATE;
// const idealMoveInDate = "2020-05-01T00:00:00+02:00";
const minMonths = parseInt(process.env.MIN_MONTHS_AVAILABLE);
exports.searchForAppartment = () => __awaiter(void 0, void 0, void 0, function* () {
    const niceMoveInMoment = moment_1.default(idealMoveInDate).format("LL");
    console.info("");
    console.info(`--- ${moment_1.default().format("LLL")} ---`);
    console.info(`Looking for an appartment with ideal move-in-date ${niceMoveInMoment}`);
    const mongoClient = new MongoClient(process.env.DB_CONNECTION, {
        useUnifiedTopology: true,
    });
    let client;
    let browser;
    let tab;
    try {
        client = yield mongoClient.connect();
        browser = yield crawl_1.launchBrowser();
        tab = yield crawl_1.openNewTab(browser);
        // console.info("Connected successfully to database");
        const db = client.db("db");
        const listingsCollection = db.collection("listings");
        const matchedListingsCollection = db.collection("matchedListings");
        // debug
        // await listingsCollection.deleteMany({});
        // await matchedListingsCollection.deleteMany({});
        const listings = yield crawl_1.getListings(tab, targetUrl);
        //console.log(listings);
        const newMatches = [];
        for (const listing of listings) {
            const dbEntry = yield listingsCollection.findOne({ id: listing.id });
            if (dbEntry) {
                console.info(`${listing.id} already in database; ending search`);
                break;
            }
            if (!dbEntry) {
                const fullListing = yield crawl_1.getFullListing(tab, listing);
                if (!fullListing)
                    continue;
                console.info(`add new listing ${listing.id} to database`);
                yield listingsCollection.insertOne(fullListing);
                const isInteresting = fullListing.isInteresting(idealMoveInDate, minMonths);
                if (isInteresting) {
                    console.info(`--- ITS A MATCH! ---`);
                    console.info(`${fullListing.url}`);
                    console.info(`--------------------`);
                    newMatches.push(new matchedListing_1.MatchedListing(fullListing));
                }
            }
        }
        console.info("Done checking all listings.");
        if (newMatches.length > 0) {
            console.info(`Logging in...`);
            yield crawl_1.login(tab);
            for (const match of newMatches) {
                console.info(`Sending notification for ${match.listing.id} ...`);
                // await applyForListing(tab, match.listing);
                yield matchedListingsCollection.insertOne(match);
                slack.send({
                    text: `Its a match! :tada:\n<${match.listing.url}|${match.listing.title}>\n:dollar: ${match.listing.price} SEK\n:arrow_right: ${moment_1.default(match.listing.moveInDate).format("LL")}`,
                });
            }
            console.info(`Done processing all new matches.`);
        }
    }
    catch (error) {
        console.error(error);
    }
    finally {
        if (browser)
            browser.close();
        if (client)
            client.close();
        console.info("----------------");
    }
});
//# sourceMappingURL=search.js.map