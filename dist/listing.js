"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Listing {
    constructor(id, title, url) {
        this.id = id;
        this.title = title;
        this.url = url;
    }
}
exports.Listing = Listing;
//# sourceMappingURL=listing.js.map